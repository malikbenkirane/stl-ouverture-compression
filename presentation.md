---
marp: true
---

# Compression d'arbres binaires de recherches
1. Représentations
2. Implémentations
3. Mémoire
4. Recherche

---

# Compression

![](images/sketches/abrc-algo.jpg)

---

# Représentations d'arbres binaires compressés

![](images/sketches/abrc.jpg)

---

```ocaml
    module type Clefs =
        sig
            type t
            val: search t -> 'a list -> 'b
            val: add t -> 'a list -> 'b -> t
        end
```

---

# 2 Implémentations

![width:800](images/sketches/abrc-table-ref.jpg)

---

## Version 1

```ocaml
    type 'a t = Node0
              | Node1 of {
                            mutable e: 'a node ;
                            mutable g: 'a t;
                            mutable d: 'a t;
                            key_g: int;
                            key_d: int
                         }
```

---

## Version 2

```ocaml
    type cbst_child = Void | Pointer of int * int | Node of int
    type 'a cbst_node = EmptyNode | CBSTNode of {
        mutable keys: 'a node;
        l: cbst_child;
        r: cbst_child
    }
    type 'a t = int * (int, 'a cbst_node) Hashtbl.t
```


---

# Stocker les clefs - Foncteurs

```
module Make (KeySet: Clefs) : S with type 'a ckeys = 'a KeySet.t
```

---


![](images/sketches/abrc.jpg)

---

- listes
- maps
- optimisations

---

# estimation complexité mémoire

--- 

# Ratio $N/n$

![width:800](images/compress_nodes_boxes.png)

---

![](images/empiric_ratio-v1-list-map.png)

---

# complexité temps de construction

![](images/compress_time_mean_vtbl.png)

---

![](images/compress_time_vref_list.png)

---

![](images/compress_time_vref_map.png)

---

# complexité temps de recherche

---

# jeux de données / expérimentations

![](images/search_bench_vref.png)

---

![](images/moy-8-structures.png)

---

# maps

![width:800](images/moy-8-map_abrc_vs_abr.png)

---

![](images/search_map_exp_vref.png)

---

# listes

![width:800](images/diff1_search_list_vref.png)

---

# optimisation (1)


![](images/ratio_vhref_float.png)


$k_1, k_2 \in \N, k_1\neq k_2\rightarrow\sin(k_1)\neq \sin(k_2)$
$sin(x+k\pi)=sin(x)$

---

# Conclusion

gitlab.com/malikbenkirane/stl-ouverture-compression