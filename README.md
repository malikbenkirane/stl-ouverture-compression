# Arbres binaires de recherche compressés

## Branches

Index des branches principales (pour les autres branches, se réferrer aux logs git):

- on appelle version 1, la version avec références et records et
- on appelle version 2, la version avec table des structures

```text

alternative             -> base de la version 2 avec tests ppx inline
alternative-hashtbl     -> complexités mémoire avec la version 2
alternative-notest      -> base de la version 2 sans les tests ppx_inline
htbl-construction_times ->  pour le calul des temps de compression
master                  -> seulement pour ce README.md
moyo                    -> base de la version 1
moyo_alternative_2      -> version 1 avec bench_time et bench_memory
moyo_hash               -> version 1 avec compactage des chemins avec sin(k)

```

## Documents (présentation et rapport)

- [présentation (md)](https://gitlab.com/malikbenkirane/stl-ouverture-compression/blob/master/presentation.md) *avec [marpit](https://github.com/marp-team/marpit)*
- [rapport (pdf)](https://gitlab.com/malikbenkirane/stl-ouverture-compression/blob/master/report.pdf)
